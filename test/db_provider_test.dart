import 'package:flutter_test/flutter_test.dart';

import 'package:todo_list/database.dart';
import 'package:todo_list/task_model.dart';

void main() {
 TestWidgetsFlutterBinding.ensureInitialized();
  group('CRUD operations', () {
    test("Task should be add", () async {

      Task newTask = Task(taskDescription: "First task");
      var length = await DbProvider.db.getTasks().then((value) => value.length);
      DbProvider.db.addTask(newTask);
      expect(await DbProvider.db.getTasks().then((value) => value.length), length + 1);
    });
    test("Task should be update", () async {

      Task task = await DbProvider.db.getTask(await DbProvider.db.getTasks().then((value) => value.length));
      task.isDone = true;
      DbProvider.db.updateTask(task);
      expect(await DbProvider.db.getTask(task.id).then((value) => value.isDone), true);
    });
    test("Task should be delete", () async {
      var length = await DbProvider.db.getTasks().then((value) => value.length);
      Task task = await DbProvider.db.getTask(length);
      DbProvider.db.deleteTask(task.id);
      expect(await DbProvider.db.getTasks().then((value) => value.length), length - 1);
    });
  });
}
